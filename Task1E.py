from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number
from floodsystem.utils import sorted_by_key


def run():
   #Build list of stations
   stations = build_station_list()

   #Print list of the rivers with the 9 most stations
   print (rivers_by_station_number(stations, 7))

if __name__ == "__main__":
    print ("*** Task 1E: CUED Part 1A Flood Warning System ***")
    run()