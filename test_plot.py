from floodsystem.stationdata import build_station_list
from floodsystem.plot import plot_water_levels
import datetime
from floodsystem.station import MonitoringStation
from floodsystem.datafetcher import fetch_measure_levels

def test_plot_water_levels():
    stations = build_station_list()
    measure_ids = {}
    for s in stations:
        measure_ids[s.name] = s.measure_id
        dt = 10
        assert s.measure_id is not None
        #dates, levels = fetch_measure_levels(measure_ids[s.name], dt=datetime.timedelta(days=dt))
       # x = plot_water_levels(s.name, dates, levels)