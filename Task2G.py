from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level, stations_level_over_threshold
from floodsystem.station import MonitoringStation

"""Uses earlier functions to determine the most at risk stations,
and uses this to produce a list of towns which are at risk, along
with their risk level."""
def run():
    #Create a list of monitoring stations
    stations = build_station_list()
    update_water_levels(stations)
    #Create empty list for final town and risk levels
    risk_level = []
    #Create dictionary with the relative water leve for each station
    rel_levels = {}
    for s in stations:
        rel_levels[s.name] = s.relative_water_level()
    #Create dictionary of stations with towns
    towns = {}
    for s in stations:
        towns[s.name] = s.town
    #Set tolerance values for risk levels
    severe = 1.6
    high = 1.2
    moderate = 0.7
    low = 0.5
    #Produce list of stations with relative water levels over low risk value
    at_risk = stations_level_over_threshold(stations, low)
    #Determine risk level of each station
    for r in at_risk:
        if towns[r[0]] not in risk_level:
            if  rel_levels[r[0]] >= severe:
                risk_level.append((towns[r[0]], "Severe"))
            elif rel_levels[r[0]] >= high:
                risk_level.append((towns[r[0]], "High"))
            elif rel_levels[r[0]] >= moderate:
                risk_level.append((towns[r[0]], "Moderate"))
            else:
                risk_level.append((towns[r[0]], "Low"))
    print(risk_level)

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()