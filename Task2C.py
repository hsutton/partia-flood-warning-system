from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list

def run():
    #Build stations list
    stations = build_station_list()

    #Print the 10 most at risk stations
    print(stations_highest_rel_level(stations, 10))

if __name__ == "__main__":
    print("*** Task 2C: CUED Part 1A Flood Warning System ***")
    run()