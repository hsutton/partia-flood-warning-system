"""Unit test for the geo module"""
from floodsystem.geo import stations_within_radius, rivers_by_station_number
from floodsystem.stationdata import build_station_list
from haversine import haversine

def test_stations_within_radius():

    stations = build_station_list()
    assert len(stations_within_radius(stations, (52.2053, 0.1218), 10)) >= 0


def test_rivers_by_station_number():

    stations = build_station_list()
    N = 10
    
    assert len(rivers_by_station_number(stations, N)) >= N
    