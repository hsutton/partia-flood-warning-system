from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    #Build list of stations
    stations = build_station_list()
    
    #Print alphabectical list of stations 10km from Cambridge city centre
    print (sorted(stations_within_radius(stations,(52.2053, 0.1218), 10)))
    

if __name__ == "__main__":
    print ("*** Task 1C: CUED Part 1A Flood Warning System ***")
    run()