from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.utils import sorted_by_key
from floodsystem.station import MonitoringStation

def test_stations_highest_rel_level():
    stations = build_station_list()
    N = 10
    most_risk = stations_highest_rel_level(stations, N)
    assert len(most_risk) == N
    n = 0
    for n in range(len(most_risk)):
        assert most_risk[n][1] is not None

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    test_stations_highest_rel_level()
