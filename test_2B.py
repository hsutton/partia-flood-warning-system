from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_level_over_threshold
from floodsystem.station import MonitoringStation

x = []
#Create list of stations
stations = build_station_list()
    #Produce list of stations with relative water levels over 0.8
p = stations_level_over_threshold(stations,0.8)
for x in p:
     print(stations[1].typical_range_consistent())