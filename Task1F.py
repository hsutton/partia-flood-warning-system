
from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.station import MonitoringStation


def run():
    """Requirements for Task 1A"""

    # Build list of stations
    s = build_station_list()
    print(sorted(inconsistent_typical_range_stations(s)))


if __name__ == "__main__":
    
    run()