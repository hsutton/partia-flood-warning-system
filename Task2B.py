from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_level_over_threshold

def run():
    #Create list of stations
    stations = build_station_list()
    #Produce list of stations with relative water levels over 0.8
    p = stations_level_over_threshold(stations,0.8)
    for n in range(len(p)):
        print(p[n][0], str(p[n][1]))

if __name__ == "__main__":
    print ("*** Task 2B: CUED Part 1A Flood Warning System ***")
    run()    