import datetime
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.station import MonitoringStation
from floodsystem.plot import plot_water_level_with_fit
import matplotlib.pyplot as plt

def run():
    #Build list of stations
    stations = build_station_list()
    measure_ids = {}
    for station in stations:
        measure_ids[station.name] = station.measure_id
    #List of stations with the 5 greatest relative water levels
    stations_at_risk = stations_highest_rel_level(stations, 5)
    for s in stations_at_risk:
        #Obtain water level data
        dt = 2
        dates, levels = fetch_measure_levels(measure_ids[s[0]], dt=datetime.timedelta(days=dt))
        #Plot
        plot_water_level_with_fit(s[0], dates, levels, 4)
       


if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()











