from .station import MonitoringStation
from .utils import sorted_by_key
from .stationdata import update_water_levels
from floodsystem.stationdata import build_station_list


def stations_level_over_threshold(stations, tol):
    b = []
    update_water_levels(stations)
    for x in stations:
        if x.relative_water_level() == None:
            continue
        elif x.typical_range_consistent() == False:
            continue
        elif x.relative_water_level() > tol:
              mytuple = (x.name, x.relative_water_level())
              b.append(mytuple)
    return sorted(b, key = lambda a: a[1], reverse = True)

def stations_highest_rel_level(stations, N):
    """ Returns a list of the N most at risk stations in the list"""
    at_risk = {}
    most_risk = []
    update_water_levels(stations)
    for s in stations:
        r = s.relative_water_level()
        if r == None:
            continue
        else:
            at_risk[s.name] = r
    at_risk = list(at_risk.items())
    sorted_by_key(at_risk, 1, True)
    for n in range(N):
        most_risk.append(at_risk[n])
    return sorted_by_key(most_risk, 1, True)