import matplotlib
import numpy as np
import matplotlib.pyplot as plt

def polyfit(dates, levels, p):
    x = matplotlib.dates.date2num(dates)
    y = x - x[0]
    p_coeff =  np.polyfit(y, levels, p)
    poly = np.poly1d(p_coeff)
    b = (poly,y)
    
    return b


