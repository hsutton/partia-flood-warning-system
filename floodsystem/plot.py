import matplotlib
import matplotlib.pyplot as plt
from .station import MonitoringStation
import numpy as np
from .analysis import polyfit
import matplotlib


def plot_water_levels(station, dates, levels):
    """Plots a graph of water level against time for a station,
    with the typical high and low levels also maked on the plot"""
    plt.plot(dates, levels)
    plt.xlabel("Date")
    plt.ylabel("Water level")
    plt.title(station)
    plt.xticks(rotation=45)
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    plt.plot(dates, levels)
    plt.xlabel("Date")
    plt.ylabel("Water level")
    plt.xticks(rotation=45)
    a = polyfit(dates, levels, p)
    plt.title(station + ', typical range = {}'.format(np.amax(levels) - np.amin(levels)))
    
    plt.plot(dates, a[0](a[1]))
    plt.show()
    