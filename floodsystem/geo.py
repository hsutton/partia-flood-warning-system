# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from haversine import haversine
from .utils import sorted_by_key
from .station import MonitoringStation

def stations_by_distance(stations, p):
    mylist = []
    for x in stations:
        distance = haversine(x.coord, p)
        mytuple = (x.name, x.town, distance)
        mylist.append(mytuple)
    return sorted_by_key(mylist,2)

def rivers_with_station(stations):
    riverslist = []
    for x in stations:
        river = x.river
        if river.startswith('River'):
            riverslist.append(river[6:])
        else:
            riverslist.append(river)
    return set(riverslist)


def stations_by_river(stations):
    riverdict = {}

    for station in stations:
        if station.river in riverdict:
            riverdict[station.river].append(station.name)
        else:
            riverdict[station.river] = [station.name]
    return riverdict


def stations_within_radius(stations, centre, r):
    """This function reurns a list of stations that are within
    a certain radius r of a centre point."""
    in_region = []
    for station in stations:
    #Find the distance between the two coordinates
        dist = haversine(centre, station.coord)
    if dist <= r:
        in_region.append(station.name)
    return in_region


def rivers_by_station_number(stations, N):
        """Determines the rivers with the most monitoring stations
        and returns them in a list sorted by the number of
        stations."""
        rivers = []
        rivers_stations = {}
        for station in stations:
                rivers.append(station.river)
        rivers.sort()
        for r in rivers:
                if r not in rivers_stations.keys():
                        rivers_stations[r] = 1
                else:
                        rivers_stations[r] += 1
        
        rivers_stations = list(rivers_stations.items())
        rivers_stations = sorted_by_key(rivers_stations, 1, True)
        most_stations = []
        i = 0
        while i < N - 1 or rivers_stations[i][1] == rivers_stations[N-1][1]:
                most_stations.append(rivers_stations[i])  
                i += 1              
        return most_stations
        

